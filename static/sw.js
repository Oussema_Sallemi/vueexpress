importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

workbox.setConfig({
  debug: true,
});
workbox.core.setCacheNameDetails({
  prefix: "test-app",
  suffix: "v1",
  precache: "test-precache",
  runtime: "test-runtime",
});

workbox.precaching.cleanupOutdatedCaches();

workbox.precaching.precacheAndRoute([{"revision":"9d448eee0c8eb60db56dcecbc5123153","url":"css/bootstrap.min.css"},{"revision":"211daf4eee015cb38fb10893c4645bbb","url":"css/cover.css"},{"revision":"d9e4e3403a7207d65992a48b20f91c01","url":"css/style.css"},{"revision":"82b9c7a5a3f405032b1db71a25f67021","url":"images/logo.png"},{"revision":"dc0d3c99b5fb815142eb775cbfcf43c0","url":"js/script.js"},{"revision":"bff97dd06233fb84ef73d7cf00fba4f3","url":"pwa.webmanifest"},{"revision":"8588ebc70d0e5371eedcce943c13a186","url":"register_service_worker.js"},{"revision":"8c76a790b7a6fde5e84e99a6f0b6a088","url":"workbox-46074162.js"}]);

workbox.core.clientsClaim();
workbox.core.skipWaiting();

workbox.routing.registerRoute(
  new RegExp("/.*"),
  new workbox.strategies.NetworkFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/"),
  new workbox.strategies.StaleWhileRevalidate({}),
  "GET"
);

// Images CacheFirst
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif|webp|bmp|svg)/,
  new workbox.strategies.CacheFirst({
    cacheName: "images",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  }),
  "GET"
);

// JS/CSS CacheFirst
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "assets",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxEntries: 1000,
      }),
    ],
  })
);

// localhost4321 CacheFirst
workbox.routing.registerRoute(
  new RegExp("http://localhost:4321/*"),
  new workbox.strategies.CacheFirst({
    cacheName: "assets-cache",
    cacheableResponse: {
      statuses: [0, 200], // Make sure 0 is included in this list.
    },
  }),
  "GET"
);

// Axios CacheFirst
workbox.routing.registerRoute(
  "https://unpkg.com/axios/dist/axios.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// JQuery CacheFirst
workbox.routing.registerRoute(
  "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// Bootstrap CacheFirst
workbox.routing.registerRoute(
  "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// Vue CacheFirst
workbox.routing.registerRoute(
  "https://cdn.jsdelivr.net/npm/vue@latest/dist/vue.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);
