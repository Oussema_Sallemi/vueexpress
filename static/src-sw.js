importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

workbox.setConfig({
  debug: true,
});
workbox.core.setCacheNameDetails({
  prefix: "test-app",
  suffix: "v1",
  precache: "test-precache",
  runtime: "test-runtime",
});

workbox.precaching.cleanupOutdatedCaches();

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);

workbox.core.clientsClaim();
workbox.core.skipWaiting();

workbox.routing.registerRoute(
  new RegExp("/.*"),
  new workbox.strategies.NetworkFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/"),
  new workbox.strategies.StaleWhileRevalidate({}),
  "GET"
);

// Images CacheFirst
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif|webp|bmp|svg)/,
  new workbox.strategies.CacheFirst({
    cacheName: "images",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  }),
  "GET"
);

// JS/CSS CacheFirst
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "assets",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxEntries: 1000,
      }),
    ],
  })
);

// localhost4321 CacheFirst
workbox.routing.registerRoute(
  new RegExp("http://localhost:4321/*"),
  new workbox.strategies.CacheFirst({
    cacheName: "assets-cache",
    cacheableResponse: {
      statuses: [0, 200], // Make sure 0 is included in this list.
    },
  }),
  "GET"
);

// Axios CacheFirst
workbox.routing.registerRoute(
  "https://unpkg.com/axios/dist/axios.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// JQuery CacheFirst
workbox.routing.registerRoute(
  "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// Bootstrap CacheFirst
workbox.routing.registerRoute(
  "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);

// Vue CacheFirst
workbox.routing.registerRoute(
  "https://cdn.jsdelivr.net/npm/vue@latest/dist/vue.min.js",
  new workbox.strategies.CacheFirst({
    cacheName: "test-cdn",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);
