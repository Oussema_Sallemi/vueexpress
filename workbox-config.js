module.exports = {
  globDirectory: "static/",
  globPatterns: ["**/*.{js,webmanifest,ico,png,jpg,md,css,vue}"],
  swDest: "static/sw.js",
  swSrc: "static/src-sw.js",
};
