const dynamicController = require("./controllers/dynamic");
const mainController = require("./controllers/main");

module.exports = (router) => {
  mainController(router);
  dynamicController(router);
};
