const ClientModel = require("../models/Client");
const CompanyModel = require("../models/Company");
const { checkGuard } = require("../middlewares");
module.exports = (router) => {
  router.get("/favicon.ico", (req, res) => res.status(204));
  router.get("/dashboard", checkGuard, async (req, res) => {
    const title = "Test Express Router + Vue Render";
    const clients = await ClientModel.find({}).lean();
    const companies = await CompanyModel.find({}).lean();
    const data = {
      title,
      users: clients,
      companies: companies,
    };
    req.vueOptions = {
      head: {
        title,
        metas: [
          {
            content: title,
          },
        ],
      },
    };
    res.renderVue("dashboard.vue", data, req.vueOptions);
  });
  router.post("/session", (req, res) => {
    req.session.token = req.body.token;
    req.session.isLoggedIN = true;
    let session = req.session;
    res.json({ session });
  });
  router.delete("/session", (req, res) => {
    req.session.destroy();
    return res.send("You are logged out!");
  });
  router.get("/", async (req, res) => {
    const title = "Login Page";
    const data = {
      title,
    };
    req.vueOptions = {
      head: {
        title,
        metas: [
          {
            content: title,
          },
        ],
      },
    };
    res.renderVue("login.vue", data, req.vueOptions);
  });
  router.get("/error", async (req, res) => {
    const title = "Error Page";
    const data = {
      title,
    };
    req.vueOptions = {
      head: {
        title,
        metas: [
          {
            content: title,
          },
        ],
      },
    };
    res.renderVue("error.vue", data, req.vueOptions);
  });
  router.get("/session", (req, res) => {
    let session = req.session;
    res.json({ session });
  });
};
