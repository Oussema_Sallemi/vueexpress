const { checkGuard } = require("../middlewares");
module.exports = (router) => {
  router.get("/:attribute/:slug", checkGuard, async (req, res) => {
    switch (req.params.attribute) {
      case "company":
        req.session.collection = "Company";
        req.session.componenet = "company";
        res.redirect(`/${req.params.slug}`);

        break;
      case "user":
        req.session.collection = "Client";
        req.session.componenet = "user";
        res.redirect(`/${req.params.slug}`);

        break;
      default:
        req.session.collection = "";
        req.session.componenet = "";
        res.redirect("/");

        break;
    }
  });
  router.get("/:slug", checkGuard, async (req, res) => {
    const payload = await require(`../models/${req.session.collection}`)
      .findOne({ url: req.params.slug })
      .exec();
    const data = {
      title: `This ${req.session.componenet} is : `,
      payload: payload.name,
    };
    res.renderVue(`${req.session.componenet}.vue`, data);
  });
};
