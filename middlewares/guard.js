function guard(req, res, next) {
  try {
    if (!req.session.isLoggedIN) {
      return res.redirect("/error");
    }
    next();
  } catch (err) {
    return res.status(500).json(err.toString());
  }
}

module.exports = guard;
