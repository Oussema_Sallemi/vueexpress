const express = require("express");
const expressVue = require("express-vue");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const routes = require("./routes");
const path = require("path");
const session = require("express-session");
const HTTP_PORT = process.env.PORT || 1234;
const dotenv = require("dotenv");
const mongoose = require("mongoose");

init();

async function init() {
  const router = express.Router();
  routes(router);

  const expressVueMiddleware = expressVue.init({
    rootPath: path.join(__dirname, "pages"),
    data: {
      test: "test",
    },
    head: {
      title: "Default title",
      scripts: [
        { src: "https://unpkg.com/axios/dist/axios.min.js" },
        { src: "/js/script.js" },
        { src: "/register_service_worker.js" },
        {
          src:
            "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
        },
      ],
      styles: [
        {
          style:
            "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        },
        {
          style: "/css/style.css",
        },
      ],
      metas: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width" },
        { name: "defaultLanguage", content: "en-US" },
        { rel: "manifest", href: "/pwa.webmanifest" },
        { rel: "apple-touch-icon", href: "/images/logo.png" },
        { name: "theme-color", content: "#047AFC" },
        {
          name: "availableLanguages",
          content:
            "en-US,bs,cak,cs,cy,de,dsb,es-AR,es-CL,es-ES,es-MX,fr,fy-NL,hsb,hu,it,ja,ka,kab,ms,nl,nn-NO,pt-BR,pt-PT,ru,sk,sl,sq,sr,sv-SE,tr,uk,zh-CN,zh-TW",
        },
      ],
    },
  });

  const app = express();
  app.use(bodyParser.json({ extended: false }));
  app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );
  app.use(cookieParser());
  dotenv.config();
  app.use(
    session({ secret: "mySecret", resave: false, saveUninitialized: false })
  );
  app.use(expressVueMiddleware);
  app.use("/assets", express.static(path.join(__dirname, "assets")));
  app.use("/", express.static(path.join(__dirname, "static")));
  mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log("connected to database")
  );
  app.use(router);

  app.listen(HTTP_PORT, function() {
    console.log(`Listening on port ${HTTP_PORT}!`);
  });
}
